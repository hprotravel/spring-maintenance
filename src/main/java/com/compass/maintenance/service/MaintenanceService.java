package com.compass.maintenance.service;

import com.compass.maintenance.provider.MaintenanceAdapter;
import com.compass.maintenance.exception.MaintenanceException;
import com.compass.maintenance.params.MaintenanceParams;
import com.compass.maintenance.params.Provider;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MaintenanceService {

  private static final String KEY_TO_STORE = ":maintenance.expires";

  @Value("${MAINTENANCE_ENABLED:false}")
  private boolean isEnabled;

  @Value("${MAINTENANCE_PROVIDER:env}")
  private Provider providerName;

  @Value("${project-name}")
  private String projectName;

  @Value("${maintenance.ttl:3600}")
  private Integer ttl;

  private final MaintenanceAdapter adapter;

  @Autowired
  public MaintenanceService(MaintenanceAdapter adapter) {
    this.adapter = adapter;
  }

  public void setMaintenanceMode(boolean maintenanceMode) {
    if (!isEnabled) {
      throw new MaintenanceException(MaintenanceParams.MAINTENANCE_DISABLED_MESSAGE.getMessage());
    }

    boolean isMaintenance = this.getExpireDateTime() != null;

    if (maintenanceMode == isMaintenance) {
      throw new MaintenanceException(
          maintenanceMode
              ? MaintenanceParams.SERVER_ALREADY_LOCKED_MESSAGE.getMessage()
              : MaintenanceParams.SERVER_ALREADY_ONLINE_MESSAGE.getMessage()
      );
    }

    if (maintenanceMode) {
      adapter.getProvider(providerName).enableMaintenance(projectName + KEY_TO_STORE, ttl);
    } else {
      adapter.getProvider(providerName).disableMaintenance(projectName + KEY_TO_STORE);
    }
  }

  public LocalDateTime getExpireDateTime() {
    return adapter.getProvider(providerName).getExpireDateTime(projectName + KEY_TO_STORE);
  }
}
