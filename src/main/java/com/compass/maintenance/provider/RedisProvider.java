package com.compass.maintenance.provider;

import com.compass.maintenance.util.Helper;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisProvider implements MaintenanceProvider {

  private StringRedisTemplate redisTemplate;

  @Autowired
  public RedisProvider setRedisTemplate(StringRedisTemplate redisTemplate) {
    this.redisTemplate = redisTemplate;

    return this;
  }

  @Override
  public void enableMaintenance(String key, int ttl) {
    redisTemplate.opsForValue().set(key, Helper.getExpireByTtl(ttl));

    redisTemplate.expire(key, ttl, TimeUnit.SECONDS);
  }

  @Override
  public void disableMaintenance(String key) {
    redisTemplate.delete(key);
  }

  @Override
  public LocalDateTime getExpireDateTime(String key) {
    String expireTime = redisTemplate.opsForValue().get(key);

    if (expireTime != null) {
      return LocalDateTime.parse(expireTime);
    }

    return null;
  }
}
