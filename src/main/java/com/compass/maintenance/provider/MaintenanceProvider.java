package com.compass.maintenance.provider;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public interface MaintenanceProvider {
  void enableMaintenance(String key, int ttl);

  void disableMaintenance(String key);

  LocalDateTime getExpireDateTime(String key);
}
