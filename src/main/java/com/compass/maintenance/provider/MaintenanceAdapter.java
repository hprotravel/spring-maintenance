package com.compass.maintenance.provider;

import com.compass.maintenance.params.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MaintenanceAdapter {

  private final EnvProvider envProvider;
  private final RedisProvider redisProvider;

  @Autowired
  public MaintenanceAdapter(EnvProvider envProvider, RedisProvider redisProvider) {
    this.envProvider = envProvider;
    this.redisProvider = redisProvider;
  }

  public MaintenanceProvider getProvider(Provider provider) {
    if (provider == Provider.REDIS) {
      return redisProvider;
    }

    return envProvider;
  }
}
