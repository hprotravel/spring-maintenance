package com.compass.maintenance.provider;

import com.compass.maintenance.params.MaintenanceParams;
import java.time.LocalDateTime;
import org.springframework.stereotype.Component;

@Component
public class EnvProvider implements MaintenanceProvider {

    @Override
    public void enableMaintenance(String key, int ttl) {
        throw new UnsupportedOperationException(
            MaintenanceParams.MAINTENANCE_CANNOT_SET_MESSAGE.getMessage()
        );
    }

    @Override
    public void disableMaintenance(String key) {
        throw new UnsupportedOperationException(
            MaintenanceParams.MAINTENANCE_CANNOT_SET_MESSAGE.getMessage()
        );
    }

    @Override
    public LocalDateTime getExpireDateTime(String key) {
        return LocalDateTime.now().plusYears(1);
    }
}
