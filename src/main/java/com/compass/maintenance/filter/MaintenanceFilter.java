package com.compass.maintenance.filter;

import com.compass.maintenance.params.MaintenanceParams;
import com.compass.maintenance.service.MaintenanceService;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MaintenanceFilter implements Filter {

  @Value("${MAINTENANCE_ENABLED:false}")
  private boolean isEnabled;

  private final MaintenanceService service;

  @Autowired
  public MaintenanceFilter(MaintenanceService service) {
    this.service = service;
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
      throws IOException, ServletException {
    if (!isEnabled) {
      filterChain.doFilter(request, response);

      return;
    }

    LocalDateTime expireDateTime = service.getExpireDateTime();
    String path = ((HttpServletRequest) request).getServletPath();

    if (
        expireDateTime != null
        && expireDateTime.isAfter(LocalDateTime.now())
        && !path.startsWith("/maintenance")
    ) {
      HttpServletResponse httpServletResponse = (HttpServletResponse) response;

      httpServletResponse.addHeader(
          HttpHeaders.RETRY_AFTER,
          expireDateTime.atZone(ZoneOffset.UTC).format(DateTimeFormatter.RFC_1123_DATE_TIME)
      );

      httpServletResponse.sendError(
          HttpServletResponse.SC_SERVICE_UNAVAILABLE,
          MaintenanceParams.LOCK_MESSAGE.getMessage()
      );
    } else {
      filterChain.doFilter(request, response);
    }

    if (path.startsWith("/maintenance")) {
      SecurityContextHolder.getContext().setAuthentication(null);
    }
  }

  @Override
  public void destroy() {

  }
}
