package com.compass.maintenance.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Helper {

  public static String getExpireByTtl(int ttl) {
    long seconds = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);

    return LocalDateTime
        .ofEpochSecond(seconds + ttl, 0, ZoneOffset.UTC)
        .toString();
  }
}
