## 0.3.0 (September 06, 2023)
  - Feature - CK-162 - Enable/disable by environment values

## 0.2.0 (July 01, 2022)
  - Improvement - CK-103 - Remove maven related fields and improve README

## 0.1.0 (March 09, 2021)
  - Update README.md
  - Initial commit

